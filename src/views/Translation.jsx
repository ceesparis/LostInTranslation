import withAuth from "../hoc/withAuth"
import TranslationForm from "../components/Translation/TranslationForm"
import TranslationResult from "../components/Translation/TranslationResult"
import { useUser } from "../context/UserContext"
import { translationAdd } from "../api/translation"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useState } from "react"
import { useContext } from "react"
import { ThemeContext } from "../context/ThemeContext"

const Translation = () => {

    const { user, setUser } = useUser()
    const [ currentTranslation, setCurrentTranslation ] = useState(null)

    // frog-experiment 
    const theme = useContext(ThemeContext)
    const frogMode = theme.state.frogMode



    const handleTranslateClicked = async translationText => {
        const transText = translationText.toLowerCase().split('').filter(char => /[a-z]/.test(char)).join("")
        
        if (!transText) {
            alert("please add letters to translate")
            return
        }

        setCurrentTranslation(transText)

        const [ error, updatedUser ] = await translationAdd(user, transText)

        if (error !== null) {
            return
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }

    return (
        <>
            <h1 className="ViewHeader">Translation Page</h1>
            <TranslationForm onTranslate={ handleTranslateClicked } frogMode={ frogMode }/> 
            { currentTranslation && <TranslationResult currentTranslation={ currentTranslation }/> }
        </>
    )
}
export default withAuth(Translation)