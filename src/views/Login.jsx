import LoginForm from "../components/Login/LoginForm"
import { useEffect } from "react"
import { useUser } from "../context/UserContext"
import { useNavigate } from "react-router"

const Login = () => {

    const { user } = useUser()
    const navigate = useNavigate()
    useEffect(() => {
        if (user !== null) {
            navigate('profile')
        }
    }, [ user, navigate ])

    return (
        <>
            <h1 className="ViewHeader">Login</h1>
            <LoginForm/>
        </>
    )
}
export default Login