import ProfileHeader from "../components/Profile/ProfileHeader"
import ProfileTranslationHistory from "../components/Profile/ProfileTranlationsHistory"
import ProfileActions from "../components/Profile/ProfileActions"
import withAuth from "../hoc/withAuth"
import { useUser } from "../context/UserContext"

const Profile = () => {

    const { user } = useUser() 

    return (
        <section className="Container">
                <h1 className="ViewHeader">Profile</h1>
            <ProfileHeader username= { user.username }/>
            <ProfileActions />
            <ProfileTranslationHistory translations = { user.translations }/>
        </section>
    )
}
export default withAuth(Profile)