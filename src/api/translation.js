import { createHeaders } from "."

const apiUrl = process.env.REACT_APP_API_URL

// make sure only the latest 10 translations are stored 
export const checkLengthTranslations = (translations) => {
    if (translations.length === 11){
        return translations.slice(-10)
    } else {
        return translations
    }
}

export const translationAdd = async (user, translation) => {
    try {
        const response = await fetch(`${apiUrl}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [ ...user.translations, translation ]
            })
        })

        if (!response.ok) {
            throw new Error('could not update the translations')
        }

        const result = await response.json()
        result.translations = checkLengthTranslations(result.translations)
        return [ null, result ]
    } catch (error) {
        return [ error.message, null ]
    }
}

export const translationClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiUrl}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if (!response.ok) {
            throw new Error('could not update translations')
        }
        const result = await response.json()
        return [ null, result ]
    } catch (error) {
        return [ error.message, null ]
    }
}

