import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { useState } from 'react'
import { storageSave } from '../../utils/storage'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'
import './Login.css'

// config for username
const usernameConfig = {
    required: true,
    minLength: 3,
    pattern: /[^ ]{3}/
}

// component
const LoginForm = () => {
    
    const {
        register,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm()

    const { setUser } = useUser()

    // local state
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)

    // event handlers
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [ error, userResponse ] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        } 
        setLoading(false)
    }

    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }

        if (errors.username.type === 'required') {
            return <span>Username is required</span>
        }

        if (errors.username.type === 'minLength') {
            return <span>Username is too short</span>
        }

        if (errors.username.type === 'pattern') {
            return <span>Username must contain at least 3 consecutive characters</span>
        }
    })()

    //render function
    return (
        <>
            <h2 className="QuestionHeader">🤖 What's your name? 🤖</h2>
            <form onSubmit={handleSubmit(onSubmit)} className="LoginForm">
                <fieldset className="LoginFormBorder">
                    <label htmlFor="username">Username: </label>
                    <input
                        type="text"
                        placeholder="enter username here"
                        {...register("username", usernameConfig)}
                    />
                    {errorMessage}
                </fieldset>

                <button type="submit" disabled={ loading }>Continue</button>
                {loading && <p>Logging in...</p>}
                {apiError && <p>{ apiError }</p>}

            </form>
        </>
    )
}

export default LoginForm