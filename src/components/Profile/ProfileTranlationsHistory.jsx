import ProfileTranslation from "./ProfileTranslation"
import { nanoid } from "nanoid"
import "./Profile.css"

const ProfileTranslationHistory = ({ translations }) => {

    const translationList = translations.map(translation => <ProfileTranslation translation={ translation } key={ nanoid() } />)

    return (
        <section className="TranslationHistorySection">
            {translationList.length > 0 ? <h4>Your translation history</h4> : <h4 className="NoHistoryHeader">No translation history yet!</h4>}
            <ul>
                { translationList }
            </ul>
        </section>
    )
}

export default ProfileTranslationHistory