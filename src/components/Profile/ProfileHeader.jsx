import "./Profile.css"

const ProfileHeader = ({ username }) =>{
    return (
        <>
            <h4 className="WelcomeUser">Welcome back, { username } </h4>
        </>
    )
}

export default ProfileHeader