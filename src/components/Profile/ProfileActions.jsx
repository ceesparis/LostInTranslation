import { Link } from "react-router-dom"
import { storageDelete, storageSave } from "../../utils/storage"
import { useUser } from "../../context/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { translationClearHistory } from "../../api/translation"
import { ThemeContext } from "../../context/ThemeContext"
import "./Profile.css"
import { useContext } from "react"

const ProfileActions = () =>{

    const { user, setUser } = useUser()   
    
    const theme = useContext(ThemeContext)
    const frogMode = theme.state.frogMode

    const toggleFrogContext = () => {
        if (frogMode) {
            theme.dispatch({ type: "NOFROGMODE" })
        } else {
            theme.dispatch({ type: "FROGMODE" })
        }
    }

    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {
            if (frogMode) {
                toggleFrogContext()
            }
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const translationClearHistoryClick = async () => {
        if (!window.confirm('Are you sure?\nThis can not be undone')) {
            return
        }

        const [ clearError ] = await translationClearHistory(user.id)

        if (clearError !== null) {
            return
        }

        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }


    return (
       <ul className="ActionList">
           <li><button><Link to="/translation" className="TranslationLinkProfile">Translate</Link></button></li>
           <li><button onClick={ translationClearHistoryClick }>Clear History</button></li>
           <li><button onClick={ handleLogoutClick }>Logout</button></li>
           { !frogMode ? <li><button onClick={ () => toggleFrogContext(theme, frogMode) }>add frogs</button></li> : <li><button onClick={ toggleFrogContext }>remove frogs</button></li> }
           { frogMode && <p>🐸</p> }
       </ul>
    )
}

export default ProfileActions