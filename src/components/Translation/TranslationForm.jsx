import {useForm} from 'react-hook-form'
import './Translation.css'

const TranslationForm = ({ onTranslate, frogMode }) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = ( { translationText } ) => {
        onTranslate(translationText)
    }

    return (
        <form onSubmit={ handleSubmit(onSubmit) } className="TranslationForm">
            <fieldset className="TranslationFieldset">
                <label htmlFor="text-to-translate">text to translate</label>
                <input type="text" {...register('translationText')}
                    placeholder="how do I say this in sign-language?" required={true}/>
            </fieldset>
            <button type="submit"> translate</button>
            { frogMode && <p>🐸</p> }
        </form>
    )

}

export default TranslationForm