import { nanoid } from 'nanoid'
import './Translation.css'

const TranslationResult = ({ currentTranslation }) => {

    const currentTranslationList = currentTranslation.split('')
    
    return (
        <div className='TranslationCard'>
            <h4>'{ currentTranslation }' translates to the following in sign language:</h4>
            {currentTranslationList.map(char => <img src={`/signs/${ char }.png`} alt={`${ char }`} key={ nanoid() } />)}
        </div>
    )

}

export default TranslationResult