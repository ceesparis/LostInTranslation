import { Link } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { useContext } from "react"
import { ThemeContext } from "../../context/ThemeContext"
import './Navbar.css'


const Navbar = () => {

    const { user } =  useUser() 
    const theme = useContext(ThemeContext)
    const frogMode = theme.state.frogMode

    return (
        <nav className="Navbar">
            <h1 className="NavbarHeader">{!frogMode ? 'Lost in Translation' : "🐸Lost in Translation🐸"}</h1>
            { user !== null &&
                <ul className="Links">
                    <li>
                        <Link to='/translation'className="TranslationLink">Translate</Link>
                    </li>
                    <li>
                        <Link to='/profile' className="ProfileLink">Profile</Link>
                    </li>
                </ul>
            }
        </nav>
    )
}

export default Navbar