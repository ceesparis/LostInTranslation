import UserProvider  from "./UserContext"
import { ThemeProvider } from "./ThemeContext"

const AppContext = ({ children }) => {

    return (
        <ThemeProvider>
            <UserProvider>
                { children }
            </UserProvider>
        </ThemeProvider>
    )
}
export default AppContext