import { createContext } from "react";
import { useReducer } from "react";
export const ThemeContext = createContext()

const initialState = { frogMode: false }

const themeReducer = (state, action) => {
    switch (action.type) {
        case "NOFROGMODE":
            return { frogMode: false }
        case "FROGMODE": 
            return { frogMode: true }
        default:
            return state
    }
}


export function ThemeProvider({ children }) {
    const [ state, dispatch ] = useReducer(themeReducer, initialState)
    return <ThemeContext.Provider value={ { state: state, dispatch: dispatch } }>{ children }</ThemeContext.Provider>
}