# Lost in Translation

This is an app which can be used to translate words into sign language. The words are translated per alphabetical character. 


## Usage

If you are new to the app, you can simply login with your new username and an account will be created automatically. If you already have an account you can login in a similar fashion. On the profile page you can see and clear your latest translations history and there is a translation page where the actual translation can be done.

## the Frog-Button

For extra practice with contexts and reducers, I created a ThemeContext which allows the user to toggle between frogmode and no-frogmode. In frogmode, some frog-emoji's are added to the profile-route, the translation-route and the navbar. The whole idea is pretty silly and there is a lot of overengineering involved, but it did help me to wrap my head around these concepts. 


## Contributing

I got the assignment and some usefull videos from Noroff School of Technology and Digital Media. For the ThemeContext [this article](https://www.section.io/engineering-education/dark-mode-for-react-app-using-context-api-and-hooks/) by Mohan Raj provided some helpfull guidance.


## License

MIT © Cees Paris